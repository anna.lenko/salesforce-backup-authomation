const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');
const Api = require('./Api');
const {
  SF_URL,
  USERNAME,
  PASSWORD,
  DATA_EXPORT_PAGE_URL,
  HOME_PAGE,
} = require('./config');
const {
  PASSCODE_PATH,
  COOKIE_PATH,
  DATA_EXPORT_PATH,
  SELECTORS,
  COMPLETE_MESSAGE,
  ERROR_MESSAGE,
} = require('./constants');

let browser;
let page;
let interval;
let isFirstIteration = true;
let fileIndex = 0;

waitForAnySelector = (selectors) =>
  new Promise((resolve) => {
    let hasFound = false;
    selectors.forEach((selector) => {
      page
        .waitForSelector(selector, { timeout: 300000 })
        .then(() => {
          if (!hasFound) {
            hasFound = true;
            resolve(selector);
          }
        })
        .catch((error) => {
          console.log(
            'Error while looking up selector ' + selector,
            error.message
          );
        });
    });
  });

checkFileDownload = async () => {
  const files = fs.readdirSync(DATA_EXPORT_PATH);
  if (files.length !== 0) {
    for (let i in files) {
      if (path.extname(files[i]) === '.ZIP') {
        console.log('--- DONE ---');
        clearInterval(interval);
        await Api.uploadFileToS3();
        fs.unlinkSync(`${DATA_EXPORT_PATH}/${files[i]}`);
        downloadFiles();
      }
    }
  }
};

startDownload = async (downloadLinks) => {
  try {
    const link = downloadLinks[fileIndex];
    if (link) {
      console.log('--- START DOWNLOADING FILE ---');
      fileIndex++;
      await link.click();
      interval = setInterval(checkFileDownload, 10000);
    } else {
      Api.sendEmailNotification(COMPLETE_MESSAGE);
    }
  } catch (error) {
    console.error(error);
    Api.sendEmailNotification(ERROR_MESSAGE);
  }
};

getDownloadLinksOnPage = async (page) => {
  await page.waitFor(15000);

  for (const frame of page.mainFrame().childFrames()) {
    if (!fs.existsSync(DATA_EXPORT_PATH)) {
      fs.mkdirSync(DATA_EXPORT_PATH);
    }

    await page._client.send('Page.setDownloadBehavior', {
      behavior: 'allow',
      downloadPath: DATA_EXPORT_PATH,
    });

    return await frame.$$(SELECTORS.DOWNLOAD_LINK, (element) => element);
  }
};

downloadFiles = async () => {
  let downloadLinks;

  if (isFirstIteration) {
    console.log('--- FIRST FILE ---');
    isFirstIteration = false;
    downloadLinks = await getDownloadLinksOnPage(page);
  } else {
    const newTab = await browser.newPage();
    const newPage = await openDataExportPage(newTab);
    downloadLinks = await getDownloadLinksOnPage(newPage);
  }

  await startDownload(downloadLinks);
};

openDataExportPage = async (page) => {
  await page.goto(DATA_EXPORT_PAGE_URL);
  await page.waitForSelector(SELECTORS.DATA_EXPORT_PAGE);
  console.log('URL:', page.url());

  return page;
};

updateCookie = async () => {
  let sfdcLv2Cookie = [];
  const cookies = await page._client.send('Network.getAllCookies');

  cookies.cookies.forEach((cookie) => {
    if (cookie.name === 'sfdc_lv2') {
      sfdcLv2Cookie.push(cookie);
    }
  });

  fs.writeFileSync(COOKIE_PATH, JSON.stringify(sfdcLv2Cookie));
};

confirmSendingCodeOnEmail = async (passcode) => {
  await page.type(SELECTORS.PASSCODE_BUTTON, passcode.toString());
  await page.click(SELECTORS.SAVE_BUTTON);
  await page.waitForSelector(HOME_PAGE);
  console.log('URL:', page.url());
};

mainWorkflow = async () => {
  await updateCookie();
  await openDataExportPage(page);
  await downloadFiles();
};

twoStepVerification = async () => {
  if (
    fs.existsSync(PASSCODE_PATH) &&
    fs.readFileSync(PASSCODE_PATH).length === 5
  ) {
    clearInterval(interval);
    const passcode = await JSON.parse(fs.readFileSync(PASSCODE_PATH));
    await confirmSendingCodeOnEmail(passcode);
    await mainWorkflow();
  }
};

startProcess = async (pageSelector) => {
  if (pageSelector === SELECTORS.PASSCODE_PAGE) {
    if (fs.readFileSync(COOKIE_PATH).length !== 0) {
      fs.writeFileSync(COOKIE_PATH, '');
    }
    interval = setInterval(twoStepVerification, 1000);
  }

  if (pageSelector === HOME_PAGE) {
    if (fs.readFileSync(PASSCODE_PATH).length !== 0) {
      fs.writeFileSync(PASSCODE_PATH, '');
    }
    await mainWorkflow();
  }
};

checkNextPage = async () => {
  const pageSelector = await waitForAnySelector([
    SELECTORS.PASSCODE_PAGE,
    HOME_PAGE,
  ]);
  console.log('URL:', page.url());
  return pageSelector;
};

salesforceLogin = async () => {
  page.setDefaultNavigationTimeout(0);

  await page.goto(SF_URL);
  await page.waitForSelector('#username');
  console.log('URL:', page.url());

  await page.type('#username', USERNAME);
  await page.type('#password', PASSWORD);
  await page.click('#Login');
};

setCookie = async () => {
  const cookie = JSON.parse(fs.readFileSync(COOKIE_PATH));
  await page.setCookie(...cookie);
};

run = async () => {
  browser = await puppeteer.launch({
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process',
    ],
    headless: false,
  });

  page = await browser.newPage();

  if (fs.readFileSync(COOKIE_PATH).length !== 0) {
    await setCookie();
  }

  await salesforceLogin();
  const pageSelector = await checkNextPage();
  await startProcess(pageSelector);

  //browser.close();
};

run();

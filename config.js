const prodConfig = {
  SF_URL: 'https://pandadoc.my.salesforce.com',
  USERNAME: 'bizops@pandadoc.com',
  PASSWORD: 'n7K1@80{',
  DATA_EXPORT_PAGE_URL:
    'https://pandadoc.lightning.force.com/lightning/setup/DataManagementExport/home',
  HOME_PAGE: 'a[href="/lightning/page/home"]',
  ACCESS_KEY_ID: '',
  SECRET_ACCESS_KEY: '',
};

const devConfig = {
  SF_URL: 'https://pandadoc-1a-dev-ed.my.salesforce.com',
  USERNAME: 'anna.lenko@pandadoc.com.dev',
  PASSWORD: 'pandadoc12',
  DATA_EXPORT_PAGE_URL:
    'https://pandadoc-1a-dev-ed.lightning.force.com/lightning/setup/DataManagementExport/home',
  HOME_PAGE: 'a[href="/lightning/setup/SetupOneHome/home"]',
  ACCESS_KEY_ID: '',
  SECRET_ACCESS_KEY: '',
};

const config = process.env.NODE_ENV === 'prod' ? prodConfig : devConfig;

module.exports = config;

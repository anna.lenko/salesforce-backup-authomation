const constants = {
  PASSCODE_PATH: 'authorization_data/passcode',
  COOKIE_PATH: 'authorization_data/cookie',
  DATA_EXPORT_PATH: 'data_export',
  BUCKET_NAME: 'pd-data-analytics',
  SELECTORS: {
    PASSCODE_PAGE: '#header',
    PASSCODE_BUTTON: '#emc',
    SAVE_BUTTON: '#save',
    DATA_EXPORT_PAGE: '#setupComponent',
    DOWNLOAD_LINK: 'td.actionColumn a',
  },
  COMPLETE_MESSAGE: {
    from: 'support@salesforce.com',
    to: 'anna.lenko@pandadoc.com, yury.patsekhin@pandadoc.com',
    subject: 'Your Organization Data Export has moved to S3 - PandaDoc',
    text: 'Your Organization Data Export has moved to S3.',
  },
  ERROR_MESSAGE: {
    from: 'support@salesforce.com',
    to: 'anna.lenko@pandadoc.com, yury.patsekhin@pandadoc.com',
    subject: 'Error',
    text: 'Something went wrong.',
  },
};

module.exports = constants;

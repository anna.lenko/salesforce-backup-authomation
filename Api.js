const AWS = require('aws-sdk');
const fs = require('fs');
const nodemailer = require('nodemailer');

const { BUCKET_NAME, DATA_EXPORT_PATH, ERROR_MESSAGE } = require('./constants');
const { ACCESS_KEY_ID, SECRET_ACCESS_KEY } = require('./config');

const sendEmailNotification = (message) => {
  let transport = nodemailer.createTransport({
    host: 'smtp.mailtrap.io',
    port: 2525,
    auth: {
      user: '1e4ecc260386b1',
      pass: 'b617f226fba06e',
    },
  });

  transport.sendMail(message, function (err, info) {
    if (err) {
      console.log(err);
    } else {
      console.log('Message sent: %s', info.messageId);
    }
  });
};

const createFolderName = async () => {
  const day = String(new Date().getDate()).padStart(2, '0');
  const month = String(new Date().getMonth() + 1).padStart(2, '0');
  const year = new Date().getFullYear();
  const currentDate = `${year} ${month} ${day}`;

  //   return `${BUCKET_NAME}/salesforce/${currentDate}`;
  return `${BUCKET_NAME}/test/${currentDate}`;
};

const uploadFileToS3 = async () => {
  try {
    AWS.config.region = 'us-west-2';
    AWS.config.update({
      accessKeyId: ACCESS_KEY_ID,
      secretAccessKey: SECRET_ACCESS_KEY,
    });

    const newFolder = await createFolderName();

    const bucket = new AWS.S3({
      params: {
        Bucket: newFolder,
      },
    });

    const files = fs.readdirSync(DATA_EXPORT_PATH);
    const fileName = files[0];
    const fileContent = fs.readFileSync(`data_export/${fileName}`);

    const params = {
      Key: fileName,
      Body: fileContent,
      ContentEncoding: 'base64',
      ContentType: 'application/zip',
      ServerSideEncryption: 'AES256',
    };

    bucket
      .putObject(params, function (err, data) {
        if (err) {
          console.log(err, err.stack);
        } else {
          console.log(data);
        }
      })
      .on('httpUploadProgress', function (progress) {
        console.log(
          Math.round((progress.loaded / progress.total) * 100) + '% done'
        );
      });
  } catch (error) {
    console.error(error);
    sendEmailNotification(ERROR_MESSAGE);
  }
};

module.exports = {
  uploadFileToS3,
  sendEmailNotification,
};
